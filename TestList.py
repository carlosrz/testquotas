from unittest import TestLoader, TestSuite
from pyunitreport import HTMLTestRunner

from Quotas import QuotasTest

quotas_test = TestLoader().loadTestsFromTestCase(QuotasTest)
testList = TestSuite([quotas_test])

kwargs = {
    "output" : '',
    "report_name": '',
    'failfast': 'true'
}

runner = HTMLTestRunner(**kwargs) 
runner.run(testList) 